#####################################################
# prom targets settings
#####################################################
appcara_prometheus_targets:
  node:
    - targets:
      - "10.16.100.88:9100" # docker node #1
      - "10.16.100.89:9100" # docker node #2
      - "10.16.100.51:9100" # appcara company website
      - "10.16.100.21:9100" # vcenter hk3 demo
      - "peplinkca1.appcara.net:9100" # peplinkca1
      - "costanalyticsdemo.appcara.net:9100" # costanalyticsdemo
      - "costanalyticsdemo-mon.appcara.net:9100" # costanalyticsdemo monitoring server
      - "app360v43.appcara.net:9100" # v43demo
      - "192.168.10.48:9100" # v43qa
      labels:
        env: hk3

appcara_prometheus_scrape_configs:
  - job_name: "prometheus"
    metrics_path: "/metrics"
    static_configs:
      - targets:
        - "localhost:9090"
  - job_name: "node"
    file_sd_configs:
      - files:
        - "/etc/prometheus/file_sd/node.yml"
  - job_name: "neteq"
    metrics_path: "/snmp"
    static_configs:
      - targets:
        - 192.168.10.1
        - 192.168.10.251
    params:
      module: [if_mib]
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: 10.16.100.89:9116  # The blackbox exporter's real hostname:port.
    file_sd_configs:
      - files:
        - "/etc/prometheus/file_sd/snmp.yml"
  - job_name: "blackbox"
    metrics_path: /probe
    params:
      module: [http_2xx]  # Look for a HTTP 200 response.
    static_configs:
      - targets:
        - http://www.appcara.com
        - https://app360v43.appcara.net
        - https://app360v43qa.appcara.hk
        - https://costanalyticsdemo.appcara.net
        - http://costanalyticsdemo-mon.appcara.net:9100
        - http://registry.appcara.net
        - https://docker.appcara.com
        - http://10.16.96.11
        - http://192.168.10.201
        - https://peplinkca1.appcara.net
        - https://10.16.100.21
        - https://192.168.10.13
        - https://10.222.0.14
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: 10.16.100.89:9115  # The blackbox exporter's real hostname:port.
    file_sd_configs:
      - files:
          - "/etc/prometheus/file_sd/blackbox.yml"

# prometheus command line options
appcara_prometheus_options:
  config.file: "/etc/prometheus/prometheus.yml"
  web.listen-address: "0.0.0.0:9090"
  web.read-timeout: "5m"
  web.max-connections: 512
#  web.external-url: ""
#  web.route-prefix: ""
#  web.local-assets: ""
#  web.user-assets: ""
#  web.enable-lifecycle:
  web.enable-admin-api:
  web.console.templates: "/etc/prometheus/consoles"
  web.console.libraries: "/etc/prometheus/console_libraries"
  storage.tsdb.path: "/prometheus"
  storage.tsdb.min-block-duration: "2h"
  storage.tsdb.retention: "15d"
#  storage.tsdb.use-lockfile:
  alertmanager.notification-queue-capacity: 10000
  alertmanager.timeout: "10s"
  query.lookback-delta: "5m"
  query.timeout: "2m"
  query.max-concurrency: 20
  log.level: "info"


#####################################################
# general settings
#####################################################
ntp_timezone: "Asia/Hong_Kong"
docker_swarm_mode: true


#####################################################
# prom docker contrainer settings
#####################################################
appcara_prometheus_image: "prom/prometheus"
appcara_prometheus_version: "v2.7.0"
appcara_prometheus_network: "prometheus_net"
appcara_prometheus_volumes:
  - volume:
      name: "prometheus-data"
      driver: local
      driver_opts:
        type: nfs
        o: addr=10.16.3.10
        device: '":/volume1/appcara-ops/docker-vol/prometheus"'
appcara_prometheus_port: 9090
appcara_prometheus_replicas: 1
appcara_prometheus_update_delay: "15s"
appcara_prometheus_update_order: "start-first"

#####################################################
# grafana docker contrainer settings
#####################################################
appcara_grafana_image: "grafana/grafana"
appcara_grafana_version: "5.4.3"
appcara_grafana_network: "{{ appcara_prometheus_network }}"
appcara_grafana_volumes:
  - volume:
      name: "grafana-data"
      driver: local
      driver_opts:
        type: nfs
        o: addr=10.16.3.10
        device: '":/volume1/appcara-ops/docker-vol/grafana"'
appcara_grafana_port: 3000
appcara_grafana_replicas: "{{ appcara_prometheus_replicas }}"
appcara_grafana_update_delay: "{{ appcara_prometheus_update_delay }}"
appcara_grafana_update_order: "{{ appcara_prometheus_update_order }}"

#####################################################
# blackbox exporter docker container settings
#####################################################
appcara_blackbox_image: "prom/blackbox-exporter"
appcara_blackbox_version: "v0.13.0"
appcara_blackbox_network: "{{ appcara_prometheus_network }}"
appcara_blackbox_port: 9115
appcara_blackbox_replicas: "{{ appcara_prometheus_replicas }}"
appcara_blackbox_update_delay: "{{ appcara_prometheus_update_delay }}"
appcara_blackbox_update_order: "{{ appcara_prometheus_update_order }}"


#####################################################
# snmp exporter docker container settings
#####################################################
appcara_snmp_image: "prom/snmp-exporter"
appcara_snmp_version: "v0.13.0"
appcara_snmp_network: "{{ appcara_prometheus_network }}"
appcara_snmp_port: 9116
appcara_snmp_replicas: "{{ appcara_prometheus_replicas }}"
appcara_snmp_update_delay: "{{ appcara_prometheus_update_delay }}"
appcara_snmp_update_order: "{{ appcara_prometheus_update_order }}"


